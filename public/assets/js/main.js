$(document).ready(function () {
    function setData(data) {
        if (data.on_off)
            $('.bulb').attr('src', 'assets/images/lb130.jpg')
        else
            $('.bulb').attr('src', 'assets/images/lb130-off.jpg')
        $('.brightness').text(data.brightness || 0)
        $('.hue').text(data.hue || 0)
        $('.saturation').text(data.saturation || 0)
        $('.colortemp').text(data.color_temp || 0)
        $('#brightness').val(data.brightness || 0)
        $('#hue').val(data.hue || 0)
        $('#saturation').val(data.saturation || 0)
        $('#colortemp').val(data.color_temp || 0)
    }
    $('.on').on('click', function () {
        $.getJSON(parseInt($('.brightness').text()) > 0 ? '/api/on/' + $('.brightness').text() : '/api/on', setData)
    })
    $('.off').on('click', function () {
        $.getJSON('/api/off', setData)
    })
    $('form#chsb').on('submit', function (event) {
        event.preventDefault()
        event.stopPropagation()
        $.getJSON('/api/color/chsb/' + $('#colortemp').val() + '/' + $('#hue').val() + '/' + $('#saturation').val() + '/' + $('#brightness').val(), setData)
    })
    $('form#colorform').on('submit', function (event) {
        event.preventDefault()
        event.stopPropagation()
        $.getJSON('/api/color/hex/' + $('#color').val().replace('#', ''), setData)
    })
    $.getJSON('/api/info', setData)
})