# TP-Link Bulb Web

## Install

    $ npm install
    or
    $ yarn

## Start

    You can provide the listening port and bulb ip address, if not are defaults

    $ npm start 3000 10.0.0.130
    or
    $ yarn start 3000 10.0.0.130

![](screen.png)

## License

MIT